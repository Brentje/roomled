﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SocketHandler : MonoBehaviour
{
    // Thread used for reading
    private Thread readThread;

    // Udp client used for comunication
    private UdpClient client;

    // Message queue
    public Color32 color;

    public ColorPicker colorPicker;
    public CustomGradientUI customGradient;
    public bool colorUpdated;
    List<string> schemes;
    public bool schemeListUpdated;


    // Port number used
    public int LocalPort = 2390;
    public int TargetPort = 2391;
    public string Host = "192.168.2.2";

    // UI elements
    public TMP_Dropdown schemeList;

    private void Awake()
    {
        // Create UDP client and connect
        client = new UdpClient(LocalPort);
        IPAddress ipAddress = Dns.GetHostEntry(Host).AddressList[0];
        IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, TargetPort);
        client.Connect(ipEndPoint);

        // Create and start thread
        readThread = new Thread(new ThreadStart(ReceiveData));
        readThread.IsBackground = true;
        readThread.Start();

        // Initialize variables
        schemes = new List<string>();
        SendCommand(Commands.RefreshSchemes);
    }

    private void Update()
    {
        if (colorUpdated)
            lock (client)
            {
                colorPicker.color = color;
                colorUpdated = false;
            }
        if (schemeListUpdated)
        {
            List<TMP_Dropdown.OptionData> listOptions = schemeList.options;
            while (schemeList.options.Count < schemes.Count)
            {
                listOptions.Add(new TMP_Dropdown.OptionData());
            }
            for (int i = 0; i < schemes.Count; i++)
            {
                listOptions[i].text = schemes[i];
            }
            schemeList.options = listOptions;

            schemeList.RefreshShownValue();
            schemeListUpdated = false;
        }
    }

    public void SendCommand(Commands command, byte[] data)
    {
        byte[] data2 = new byte[data.Length + 1];
        data2[0] = (byte)command;
        Array.Copy(data, 0, data2, 1, data.Length);
        client.Send(data2, data2.Length);
        Debug.Log($"Data {data[0]} - {data[1]} - {data[2]}");
    }
    public void SendCommand(Commands command, byte a, byte b, byte c)
    {
        byte[] data2 = { (byte)command, a, b, c };
        client.Send(data2, data2.Length);
    }
    public void SendCommand(Commands command, byte a)
    {
        byte[] data2 = { (byte)command, a };
        client.Send(data2, data2.Length);
    }
    public void SendCommand(Commands command)
    {
        byte[] data2 = { (byte)command };
        client.Send(data2, data2.Length);
    }
    public void SendCommand(Commands command, byte a, byte b, byte c, byte d)
    {
        byte[] data2 = { (byte)command, a, b, c, d };
        client.Send(data2, data2.Length);
    }

    public void ColorChanged(Color color)
    {
        Color32 color32 = color;
        byte[] sendBytes = { color32.r, color32.g, color32.b };
        SendCommand(Commands.SetColor, sendBytes);
    }

    public void EnabledChanged(bool enabled)
    {
        SendCommand(Commands.SetOnOff, (byte)(enabled ? 1 : 0));
    }

    public void OnGradientUpdated()
    {
        CustomGradient g = customGradient.gradient;
        for (int i = 0; i < g.NumKeys; i++)
        {
            SendCommand(Commands.SetPaletteColorAtIndex, (byte)(g.GetKey(i).Time * 255), ((Color32)g.GetKey(i).Color).r, ((Color32)g.GetKey(i).Color).g, ((Color32)g.GetKey(i).Color).b);
        }
        SendCommand(Commands.CreateColorPalette);
    }

    public void OnGradientKeyCountUpdated(byte size)
    {
        SendCommand(Commands.ResizeColorPalette, size);
    }

    private void ReceiveData()
    {
        Debug.Log("Listning to device");
        while (true)
        {
            try
            {
                IPEndPoint anyRemoteIP = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = client.Receive(ref anyRemoteIP);

                if (data.Length > 0)
                {
                    switch ((Commands)data[0])
                    {
                        case Commands.RefreshSchemes:
                            {
                                if (data.Length > 3)
                                {
                                    while (schemes.Count <= data[1])
                                    {
                                        schemes.Add("");
                                    }

                                    // Copy text bytes to new array
                                    byte[] textbytes = new byte[data.Length - 2];
                                    Array.Copy(data, 2, textbytes, 0, data.Length - 2);

                                    // Convert text bytes to ascii and change what the dropdown says
                                    string name = Encoding.ASCII.GetString(textbytes);
                                    schemes[data[1]] = name;
                                    schemeListUpdated = true;
                                }

                                break;
                            }
                        case Commands.SetColor:
                            {
                                if (data.Length > 3)
                                {
                                    lock (client)
                                    {
                                        color = new Color32(data[1], data[2], data[3], 255);
                                    }
                                    colorUpdated = true;
                                }
                                break;
                            }
                        default:
                        break;
                    }
                }
            }
            catch (Exception exeption)
            {
                Debug.Log(exeption);
            }
        }
    }

    public void ChangeScheme(int index)
    {
        SendCommand(Commands.SetScheme, (byte)index);
    }

    public void ChangeSpeed(Single speed)
    {
        SendCommand(Commands.SetSpeed, (byte)speed);
    }

    public void ChangeBrightness(Single brightness)
    {
        SendCommand(Commands.SetBrightness, (byte)brightness);
    }

    public void ChangeEnable(bool enabled)
    {
        SendCommand(Commands.SetOnOff, (byte)(enabled ? 1 : 0));
    }

    private void OnApplicationQuit()
    {
        stopThread();
    }

    private void stopThread()
    {
        // Stop reading UDP messages
        if (readThread.IsAlive)
        {
            readThread.Abort();
        }
        client.Close();
    }

    public void UpdateSchemeList()
    {
        SendCommand(Commands.RefreshSchemes);
        IPAddress ipAddress = Dns.GetHostEntry(Host).AddressList[0];
        IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, TargetPort);
        client.Connect(ipEndPoint);
    }

    public void SetColorRed() { ColorChanged(Color.red); colorPicker.color = Color.red; }
    public void SetColorYellow() { ColorChanged(new Color(1, 1, 0)); colorPicker.color = new Color(1, 1, 0); }
    public void SetColorGreen() { ColorChanged(Color.green); colorPicker.color = Color.green; }
    public void SetColorCyan() { ColorChanged(Color.cyan); colorPicker.color = Color.cyan; }
    public void SetColorBlue() { ColorChanged(Color.blue); colorPicker.color = Color.blue; }
    public void SetColorPurple() { ColorChanged(Color.magenta); colorPicker.color = Color.magenta; }
}
