﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelPicker : MonoBehaviour
{
    public RectTransform[] panels;
    public int[] panelEnums;

    public void Start()
    {
        if (panels == null)
            panels = new RectTransform[0];
    }
    public void SchemeChanged(int scheme)
    {
        int panelIndex = panelEnums[scheme];
        if(panelIndex < 0 || panelIndex >= panels.Length)
        {
            for (int i = 0; i < panels.Length; i++)
            {
                panels[i].gameObject.SetActive(false);
            }
            return;
        }
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].gameObject.SetActive(i == panelIndex);
        }
    }
}
