﻿public enum Commands
{
    // Set the current color (does nothing when using a palette based scheme)
    SetColor,
    // Change the current scheme
    SetScheme,
    // Change the brightness
    SetBrightness,
    // Change the speed (0 - 9)
    SetSpeed,
    // Turn led strip on of off
    SetOnOff,
    // Request the scheme names (the controller can request this from the led strip)
    RefreshSchemes,

    // Create color palette from given data
    CreateColorPalette,
    // Resize the palette data (clears data)
    ResizeColorPalette,
    // Sets palette data at the given index to the given color (index,r,g,b)
    SetPaletteColorAtIndex,
}