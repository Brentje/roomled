﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CustomGradientUI : MonoBehaviour
{
    public CustomGradient gradient;
    public List<CustomGradientKeyUI> keys;
    public RawImage previewImage;
    public RectTransform keysParent;
    public CustomGradientKeyUI prefab;
    public int selectedKeyIndex;
    public ColorPicker colorPicker;

    public GradientChangedEvent OnGradientUpdated;
    public GradientKeyCountChangedEvent OnKeyCountUpdated;

    private void Start()
    {
        previewImage.texture = gradient.GetTexture((int)previewImage.rectTransform.rect.width);
        for (int i = 0; i < gradient.NumKeys; i++)
        {
            CustomGradientKeyUI key = Instantiate(prefab, keysParent);
            keys.Add(key);
            key.manager = this;
            Rect keyRect = new Rect(keysParent.rect.x + keysParent.rect.width * gradient.GetKey(i).Time, keysParent.rect.y / 2f, 0, 0);
            ((RectTransform)keys[i].transform).localPosition = keyRect.position;
            key.UpdateColor(gradient.GetKey(i).Color);
            key.keyIndex = i;
        }
        colorPicker.color = keys[selectedKeyIndex].color;
        OnKeyCountUpdated.Invoke(5);
    }

    public void Update()
    {
        previewImage.texture = gradient.GetTexture((int)previewImage.rectTransform.rect.width);
    }

    public void UpdateKeyTime(int index, float mousex)
    {
        float halveWidth = keysParent.rect.width / 2f;
        float x = Mathf.Clamp(mousex - halveWidth - 50, keysParent.rect.x, keysParent.rect.xMax);
        Rect keyRect = new Rect(x, keysParent.rect.y / 2f, 0, 0);
        ((RectTransform)keys[index].transform).localPosition = keyRect.position;


        for (int i = 0; i < gradient.NumKeys; i++)
        {
            gradient.RemoveKey(i);
        }

        CustomGradient.ColorKey[] newKeys = new CustomGradient.ColorKey[keys.Count];
        for (int i = 0; i < keys.Count; i++)
        {
            float time = Mathf.InverseLerp(0, keysParent.rect.width, keys[i].transform.position.x - 50);
            newKeys[i] = new CustomGradient.ColorKey(keys[i].color, time);
        }
        gradient.SetNewKeys(newKeys);
        keys = keys.OrderBy(obj => obj.transform.position.x).ToList();
        for (int i = 0; i < keys.Count; i++)
        {
            keys[i].keyIndex = i;
        }

        //OnGradientUpdated.Invoke(gradient);
    }

    public void SelectKey(int index)
    {
        selectedKeyIndex = index;
        colorPicker.color = keys[index].color;
    }

    public void ReleaseKey(int index)
    {
        OnGradientUpdated?.Invoke(gradient);
    }

    public void UpdateColor(Color color)
    {
        keys[selectedKeyIndex].UpdateColor(color);
        gradient.UpdateKeyColor(selectedKeyIndex, color);
        //OnGradientUpdated.Invoke(gradient);
    }

    public void OnColorDoneChanging(Color color)
    {
        OnGradientUpdated?.Invoke(gradient);
    }

    [System.Serializable] public class GradientChangedEvent : UnityEvent<CustomGradient> { }
    [System.Serializable] public class GradientKeyCountChangedEvent : UnityEvent<byte> { }
}
