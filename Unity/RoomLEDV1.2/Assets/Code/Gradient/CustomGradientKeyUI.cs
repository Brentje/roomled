﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomGradientKeyUI : MonoBehaviour
{
    public int keyIndex;
    public Color color;
    private RawImage image;
    public CustomGradientUI manager;

    public void UpdateColor(Color color)
    {
        image.color = color;
        this.color = color;
    }

    private void Awake()
    {
        image = GetComponent<RawImage>();
    }

    public void OnDrag(BaseEventData data)
    {
        manager.UpdateKeyTime(keyIndex, Input.mousePosition.x);
    }

    public void OnSelect()
    {
        manager.SelectKey(keyIndex);
    }

    public void OnRelease()
    {
        manager.ReleaseKey(keyIndex);
    }
}
