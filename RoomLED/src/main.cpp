// Includes
#include <Arduino.h>
#include <FastLED.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>

#include "Commands.h"
#include "Settings.h"
#include "Scheme.h"
#include "Schemes/SingleColorScheme.h"
#include "Schemes/RainbowScheme.h"
#include "Schemes/PaletteScheme.h"

// Defines
#define DATAPIN 12
#define LEDPIN 13

// WiFi variables
IPAddress localIP(192, 168, 2, 220);
unsigned int localPort = 2390;
unsigned int targetPort = 2391;

char ssid[] = "Project: RoomLED"; // Network SSID
char pass[] = "Room_LED";         // network password (WPA)

WiFiUDP udpClient;
int status = WL_IDLE_STATUS;

char packetBuffer[20]; // buffer to hold incoming packet

// FastLED variables
CRGB strip[LED_COUNT];

// Strip settings
Settings *settings = new Settings();
Scheme *scheme;
Scheme *schemes[]{
    new SingleColorScheme(strip, settings),
    new RainbowScheme(strip, settings),
    new PaletteScheme(strip, settings),
};
#define SCHEME_COUNT 3 // <----- DONT FORGET WHEN CHANGING THE SCHEMES ARRAY

void ChangeScheme(uint8_t index)
{
  if (scheme != nullptr)
    scheme->OnExit();

  if (index >= 0 && index < SCHEME_COUNT)
  {
    scheme = schemes[index];
    scheme->Init();
    scheme->OnStart();
    scheme->ApplyBrightness();
    scheme->ApplyColor();
    scheme->ApplyEnabledState();
    scheme->ApplySpeed();
  }
}

void setup()
{
  Serial.begin(9600);

  // Pin 13 (Builtin LED) is used as error indicator
  pinMode(LEDPIN, OUTPUT);

  digitalWrite(LEDPIN, true);
  delay(1000);
  digitalWrite(LEDPIN, false);

  // Check for WiFi module
  if (WiFi.status() == WL_NO_MODULE)
  {
    Serial.println("No WiFi module found");

    while (true)
    {
      digitalWrite(LEDPIN, true);
      delay(300);
      digitalWrite(LEDPIN, false);
      delay(2000);
    }
  }

  // Check for firmware update
  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION)
  {
    Serial.println("Please upgrade the firmware");
  }

  // Configure WiFi
  WiFi.config(localIP);

  // Print the network name (SSID);
  Serial.print("Creating access point named: ");
  Serial.println(ssid);
  Serial.print("Device Ip address: ");
  Serial.println(localIP);

  // Create open network. Change this line if you want to create an WEP
  // network:
  status = WiFi.beginAP(ssid, pass);
  if (status != WL_AP_LISTENING)
  {
    Serial.println("Creating access point failed");

    // Stop and display error code
    while (true)
    {
      digitalWrite(LEDPIN, true);
      delay(300);
      digitalWrite(LEDPIN, false);
      delay(300);
      digitalWrite(LEDPIN, true);
      delay(300);
      digitalWrite(LEDPIN, false);
      delay(2000);
    }
  }

  // Wait 5 seconds for a connection to be made
  for (size_t i = 0; i < 5; i++)
  {
    digitalWrite(LEDPIN, true);
    delay(500);
    digitalWrite(LEDPIN, false);
    delay(500);
  }

  // Begin UDP
  udpClient.begin(localPort);

  // Setup FastLED
  FastLED.addLeds<WS2812, DATAPIN, GRB>(strip, LED_COUNT);
  FastLED.setMaxRefreshRate(60);
  ChangeScheme(0);

  // TODO: Potential relay for power saving
}

// Send command to the controler
void SendMessage(Commands command, uint8_t first = 0, uint8_t second = 0,
                 uint8_t third = 0)
{
  udpClient.beginPacket(udpClient.remoteIP(), udpClient.remotePort());
  uint8_t buffer[4]{(uint8_t)command, first, second, third};
  udpClient.write(buffer, 4);
  udpClient.endPacket();
}

// Send command with text to the controler
void SendMessage(Commands command, char *text)
{
  udpClient.beginPacket(udpClient.remoteIP(), udpClient.remotePort());
  udpClient.write((char)command);
  udpClient.write(text);
  udpClient.endPacket();
}

// Send command with text at index to the controler
void SendMessage(Commands command, uint8_t index, char *text)
{
  udpClient.beginPacket(udpClient.remoteIP(), udpClient.remotePort());
  udpClient.write((char)command);
  udpClient.write(index);
  udpClient.write(text);
  udpClient.endPacket();
}

void loop()
{
  // put your main code here, to run repeatedly:

  // Check for available packets
  int packetSize = udpClient.parsePacket();
  if (packetSize)
  {
    int len = udpClient.read(packetBuffer, 20);

    if (len > 0)
    {
      packetBuffer[len] = 0;
    }

    switch ((Commands)packetBuffer[0])
    {
    case (Commands::SetColor):
    {
      // Create color from data
      settings->color[0] = packetBuffer[1];
      settings->color[1] = packetBuffer[2];
      settings->color[2] = packetBuffer[3];

      // Change color to new color
      scheme->ApplyColor();

      break;
    }
    case (Commands::SetBrightness):
    {
      // Change brightness to new brightness
      settings->brightness = packetBuffer[1];
      scheme->ApplyBrightness();

      break;
    }
    case (Commands::SetScheme):
    {
      // Change scheme
      ChangeScheme(packetBuffer[1]);
      break;
    }
    case (Commands::SetSpeed):
    {
      // Change speed
      settings->speed = packetBuffer[1];
      scheme->ApplySpeed();

      break;
    }
    case (Commands::SetOnOff):
    {
      settings->enabled = packetBuffer[1];
      scheme->ApplyEnabledState();

      // TODO: Disable the power relay to save power
      break;
    }
    case (Commands::SetPaletteColorAtIndex):
    {
      // TODO: Change Palette
      settings->colorPalette.SetColorAtIndex(packetBuffer[1], packetBuffer[2], packetBuffer[3], packetBuffer[4]);
      Serial.print("CPC - i:");
      Serial.print((byte)packetBuffer[1]);
      Serial.print(" r:");
      Serial.print((byte)packetBuffer[2]);
      Serial.print(" g:");
      Serial.print((byte)packetBuffer[3]);
      Serial.print(" b:");
      Serial.println((byte)packetBuffer[4]);
      break;
    }
    case (Commands::ResizeColorPalette):
    {
      settings->colorPalette.Resize(packetBuffer[1]);
      Serial.print("RCP - size:");
      Serial.println((byte)packetBuffer[1]);
      break;
    }
    case (Commands::CreateColorPalette):
    {
      settings->colorPalette.CreatePaletteFromData();
      scheme->ApplyPalette();
      Serial.println("CCP");
      break;
    }
    case (Commands::RefreshSchemes):
    {
      for (size_t i = 0; i < SCHEME_COUNT; i++)
      {
        SendMessage(Commands::RefreshSchemes, i, schemes[i]->GetSchemeName());
      }
      break;
    }

    default:
      break;
    }
  }
  if (settings->enabled)
  {
    // Update scheme
    scheme->OnUpdate();
  }
}