#ifndef RAINBOWSCHEME
#define RAINBOWSCHEME

#include "Scheme.h"
#include "math.h"

class RainbowScheme : public Scheme
{
public:
    RainbowScheme(CRGB *strip, Settings *settings)
        : Scheme(strip, settings) {}

    virtual void OnUpdate()
    {
        // Increse counter by one and check if it is high enough to update the led strip
        counter++;

        if (counter >= speeds[settings->speed])
        {
            if (counter == 0)
                gHue += 2;
            else
                gHue++;
            fill_rainbow(strip, LED_COUNT, gHue, 1);
            counter = 0;
        }
        FastLED.show();
    }

    virtual char *GetSchemeName() { return "Rainbow"; }

private:
    uint8_t gHue;
    int counter;
    int speeds[10]{52, 32, 20, 12, 8, 5, 3, 2, 1, 0};
};

#endif