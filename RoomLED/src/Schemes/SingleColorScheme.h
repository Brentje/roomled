#ifndef SINGLECOLORSCHEME
#define SINGLECOLORSCHEME

#include "Scheme.h"

class SingleColorScheme : public Scheme
{
public:
  SingleColorScheme(CRGB *strip, Settings *settings)
      : Scheme(strip, settings) {}

  virtual void ApplyColor()
  {
    FastLED.showColor(settings->color);
  }
  virtual void ApplyBrightness()
  {
    FastLED.setBrightness(settings->brightness);
    FastLED.showColor(settings->color);
  }

  virtual char *GetSchemeName() { return "Solid color"; }
};

#endif