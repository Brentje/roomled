#ifndef PALETTESCHEME
#define PALETTESCHEME

#include "Scheme.h"

class PaletteScheme : public Scheme
{
public:
    PaletteScheme(CRGB *strip, Settings *settings)
        : Scheme(strip, settings) {
            this->palette = settings->colorPalette.GetPalette();
        }

    virtual void OnUpdate()
    {
        uint8_t brightness = 255;
        for (int i = 0; i < LED_COUNT; i++)
        {
            strip[i] = ColorFromPalette((*palette), (uint8_t)i, brightness, LINEARBLEND);
        }
        FastLED.show();
    }

private:
    CRGBPalette256 *palette;
};

#endif