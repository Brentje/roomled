#ifndef SCHEME
#define SCHEME

#include "Settings.h"

class Scheme {
 protected:
  Settings *settings;
  CRGB *strip;

 public:
  Scheme(CRGB *strip, Settings *settings) {
    this->settings = settings;
    this->strip = strip;
  }
	
  virtual void Init() {}
  virtual void OnStart() {}
  virtual void OnUpdate() {}
  virtual void OnExit() {}
  virtual char *GetSchemeName() { return "Empty scheme"; }

  virtual void ApplyColor() {}
  virtual void ApplyPalette() {}
  virtual void ApplyBrightness() { FastLED.setBrightness(settings->brightness); }
  virtual void ApplySpeed() {}
  virtual void ApplyEnabledState() {
    if (!settings->enabled) FastLED.showColor(CRGB::Black);
  }
  virtual ~Scheme(){}
};

#endif