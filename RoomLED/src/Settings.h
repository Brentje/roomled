#ifndef SETTINGS
#define SETTINGS

#include "FastLED.h"
#include "GradientPalette.h"

#define LED_COUNT 300

class Settings {
 private:
  /* data */
 public:
  CRGB color = CRGB::White;
  GradientPalette colorPalette;
  int8_t brightness = 255;
  int8_t speed = 1;
  bool enabled = true;
};

#endif