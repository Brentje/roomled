#ifndef GRADIENTPALETTE
#define GRADIENTPALETTE

#include "FastLED.h"

struct GradientPaletteKey
{
    union {
        struct
        {
            union {
                uint8_t i;
                uint8_t index;
            };
            union {
                uint8_t r;
                uint8_t red;
            };
            union {
                uint8_t g;
                uint8_t green;
            };
            union {
                uint8_t b;
                uint8_t blue;
            };
        };
        uint8_t raw[4];
    };
};

int sort_desc(const void *cmp1, const void *cmp2)
{
    // Need to cast the void * to int *
    uint8_t a = ((GradientPaletteKey *)cmp1)->i;
    uint8_t b = ((GradientPaletteKey *)cmp2)->i;
    // The comparison
    return a > b ? -1 : (a < b ? 1 : 0);
    // A simpler, probably faster way:
    //return b - a;
}

class GradientPalette
{
public:
    GradientPalette() {}
    void Resize(uint8_t size)
    {
        data = new byte[size * 4];
        palette = new CRGBPalette256();
    }
    void SetColorAtIndex(uint8_t index, uint8_t r, uint8_t g, uint8_t b)
    {
        uint16_t start = index * 4;
        if (index < sizeof(data) / sizeof(data[0]))
        {
            data[start] = index;
            data[start + 1] = r;
            data[start + 2] = g;
            data[start + 3] = b;
        }
    }
    void CreatePaletteFromData()
    {
        /*uint8_t size = sizeof(data) / sizeof(data[0]);
        qsort(data, size, sizeof(data[0]), sort_desc);
        byte *converted = new byte[sizeof(data)];
        uint16_t startIndex = 0;
        for (uint8_t i = 0; i < size; i++)
        {
            startIndex = i * 4;
            converted[startIndex] = data[i].index;
            converted[startIndex + 1] = data[i].r;
            converted[startIndex + 2] = data[i].g;
            converted[startIndex + 3] = data[i].b;
        }*/

        palette->loadDynamicGradientPalette(data);
    }
    CRGBPalette256 *GetPalette()
    {
        return palette;
    }

private:
    CRGBPalette256 *palette;
    byte *data;
};

#endif